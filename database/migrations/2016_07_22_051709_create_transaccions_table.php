<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaccionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transacciones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tipo_id')->unsigned();
            $table->date('fecha');
            $table->integer('cuenta_id')->unsigned();
            $table->integer('categoria_id')->unsigned();
            $table->integer('usuario_id')->unsigned();
            $table->string('detalle');
            $table->double('monto');
            $table->integer('traslado')->default(0);
            $table->foreign('categoria_id')->references('id')->on('categorias');
            $table->foreign('cuenta_id')->references('id')->on('cuentas');
            $table->foreign('tipo_id')->references('id')->on('tipos');
            $table->foreign('usuario_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transacciones');
    }
}
