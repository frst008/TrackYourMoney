<?php

use Illuminate\Database\Seeder;
use App\Tipo;
class TiposTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $tipos = [
        ['descripcion' => 'Ingreso'],
        ['descripcion' => 'Gasto'],
      ];

      foreach ($tipos as $tipo) {
          Tipo::create($tipo);
      }
    }
}
