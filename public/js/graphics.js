// http://code.highcharts.com/zips/Highcharts-4.0.1.zip

function parseArray(data) {
  var array = [];
  for (var i = 0; i < data.length; i++) {
    array.push([data[i].categoria_nombre, parseFloat(data[i].percentage)]);
  }
  return array;
}

function parseArraycat(data) {
  var array = [];
  for (var i = 0; i < data.length; i++) {
    array.push([data[i].cat_id]);
  }
  return array;
}

function makeGraphic(dataGraphic, graphicTitle, type,tipo) {
  $('#graphic').empty();
  switch(type) {
    default:
    case 1:
    pieChart(dataGraphic, graphicTitle, type,tipo);
    break;
    case 2:
    circleDonut(dataGraphic, graphicTitle, type,tipo);
    break;
    case 3:
    semiCircleDonut(dataGraphic, graphicTitle, type,tipo);
  }
}

function pieChart(dataGraphic, graphicTitle, type,tipo) {
  $('#graphic').highcharts({
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false
    },
    title: {
      text: graphicTitle,
      align: 'center',
      verticalAlign: 'top'
    },
    tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.percentage:.1f} %',
          style: {
            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
          }
        }
      }
    },
    series: [{
      type: 'pie',
      name: 'Browser share',
      data: parseArray(dataGraphic),
      point:{
        events:{
          click: function (event) {
              parseArraycat(dataGraphic).forEach(function(entry) {
            $.getJSON("/categorias/graphic-data/"+tipo+"/"+entry, function(data) {
              if (data.length != 0) {
                makeGraphic(
                  data,
                  graphicTitle,
                  type,
                 tipo);
              }
              });
              });
            }
          }
        }
      }]
    });
  }

  function circleDonut(dataGraphic, graphicTitle, type,tipo) {
    $('#graphic').highcharts({
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false
      },
      title: {
        text: graphicTitle,
        align: 'center',
        verticalAlign: 'top'
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
            style: {
              color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
            }
          }
        }
      },
      series: [{
        type: 'pie',
        name: 'Browser share',
        innerSize: '70%',
        data: parseArray(dataGraphic),
        point:{
          events:{
            click: function (event) {
                parseArraycat(dataGraphic).forEach(function(entry) {
              $.getJSON("/categorias/graphic-data/"+tipo+"/"+entry, function(data) {
                if (data.length != 0) {
                  makeGraphic(
                    data,
                    graphicTitle,
                    type,
                   tipo);
                }
                });
                });
              }
            }
          }
      }]
    });
  }

  function semiCircleDonut(dataGraphic, graphicTitle, type,tipo) {
    $('#graphic').highcharts({
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false
      },
      title: {
        text: graphicTitle,
        align: 'center',
        verticalAlign: 'top'
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
            style: {
              color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
            }
          },
          startAngle: -90,
          endAngle: 90,
          center: ['50%', '75%']
        }
      },
      series: [{
        type: 'pie',
        name: 'Browser share',
        innerSize: '50%',
        data: parseArray(dataGraphic),
        point:{
          events:{
            click: function (event) {
                parseArraycat(dataGraphic).forEach(function(entry) {
              $.getJSON("/categorias/graphic-data/"+tipo+"/"+entry, function(data) {
                if (data.length != 0) {
                  makeGraphic(
                    data,
                    graphicTitle,
                    type,
                   tipo);
                }
                });
                });
              }
            }
          }
      }]
    });
  }
