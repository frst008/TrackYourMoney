@extends('layouts.app')
@section('content')
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
<div class="col-lg-7 col-md-offset-2">
  <div class="panel panel-default">
    <div class="panel-heading">Crear Moneda</div>
    <div class="panel-body">
      <form class="form-horizontal" role="form" action="/monedas" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
          <label class="col-md-3 control-label" for="name">Name</label>
          <div class="col-md-9">
            <select name="moneda_id" class="selectpicker" data-live-search="true" data-width="100%">
              @foreach($monedas as $moneda)
              <option data-tokens="{{$moneda->descripcion}}-{{$moneda->simbolo}}" value="{{$moneda->id}}">{{$moneda->descripcion}}-{{$moneda->simbolo}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-3 control-label" for="tasa">Tasa</label>
          <div class="col-md-9">
            <input name="tasa" type="number" class="form-control" step="any" id="tasa" />
          </div>
        </div>
        @if($principal == 0)
        <div class="form-group">
          <label class="col-md-3 control-label" for="moneda_principal">Definir principal</label>
          <div class="col-md-9">
            <div class="checkbox">
              <label>
                <input type="checkbox" name="moneda_principal" id="moneda_principal"/>
              </label>
            </div>
          </div>
        </div>
        @endif
        <div class="form-group">
          <div class="col-md-12">
            <button type="submit" class="btn btn-primary btn-md pull-right">Guardar</button>
          </div>
        </div>
      </form>
      <a href="{{ URL::previous() }}"><button class="btn btn-warning pull-right" >Volver</button></a>
    </div>
  </div>
</div>
<script type="text/javascript">
$(function () {
    $('#tasa').show();
    $('#moneda_principal').on('click', function () {
        if ($(this).prop('checked')) {
            $('#tasa').hide();
        } else {
            $('#tasa').fadeIn();
        }
    });
});
</script>
@endsection
