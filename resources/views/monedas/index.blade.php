@extends('layouts.app')
@section('content')
@foreach($usuario_monedas as $usuario_moneda)
<div class="col-sm-6 col-md-4">
  <div class="thumbnail">
    <div class="caption">
      <h3 class="text-center">{{$usuario_moneda->moneda->descripcion}}</h3>
      <ul>
        <h5><li><strong>Nombre Corto:</strong> {{$usuario_moneda->moneda->nombre_corto}}</li></h5>
        <h5><li><strong>Simbolo:</strong> {{$usuario_moneda->moneda->simbolo}}</li></h5>
        <h5><li><strong>Tasa:</strong> {{$usuario_moneda->tasa}}</li></h5>
        <h5><li><strong>Moneda Principal:</strong> {{$usuario_moneda->moneda_principal == 1 ? 'Si' : 'No'}}</li></h5>
      </ul>
      <div class="text-center">
        <a class="btn btn-info" href="monedas/{{$usuario_moneda->id}}"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
        <a class="btn btn-warning" href="monedas/{{$usuario_moneda->id}}/edit"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
        {!! Form::open(['method' => 'DELETE','route' => ['monedas.destroy', $usuario_moneda->id],'style'=>'display:inline']) !!}
        <button type="submit" class="btn btn-danger">
          <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
        </button>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@endforeach
<div class="text-center">
  <a href="{{ url('monedas/create') }}" class="btn btn-default"><i class="fa fa-plus-circle fa-3x" aria-hidden="true"></i><h3>Agregar Nueva Moneda</h3></a>
</div>
<div class="text-center">
{{ $usuario_monedas->links() }}
</div>

@endsection
