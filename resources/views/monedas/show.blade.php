@extends('layouts.app')
@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Ver Moneda</div>
				<div class="panel-body">
					@if(is_null($usuario_moneda))
					<h1>Mostrando Moneda</h1>
					<p>La Moneda solicitada no existe</p>
					@else
					<h1>Mostrando: {{ $usuario_moneda->moneda->descripcion }}</h1>
					<div class="row">
						<div class="col-md-5">
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1">Moneda: </span>
								<input readonly="true" type="text" class="form-control" value="{{ $usuario_moneda->moneda->descripcion }}">
							</div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-5">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon2">Tasa: </span>
						<input readonly="true" type="text" class="form-control" value="{{ $usuario_moneda->tasa }}">
					</div>
				</div>
			</div>
					@endif
				</div>
			</div>
			<div class="row">
				<div class="col-md-5">
					<a href="{{ url('/monedas') }}"
					class='btn btn-default btn-sm'>
					Ver todas las monedas
				</a>
			</div>
		</div>
	</div>
</div>
</div>
</div>

@endsection
