@extends('layouts.app')
@section('content')
@foreach($cuentas as $cuenta)
<div class="col-sm-6 col-md-4">
  <div class="thumbnail">
    <img src="image/{{$cuenta->icono}}" alt="" />
    <div class="caption">
      <h3 class="text-center">{{$cuenta->nombre_corto}}</h3>
      <ul>
        <li>{{$cuenta->descripcion}}</li>
        <li>{{$cuenta->saldo_inicial}}</li>
        <li>{{$cuenta->moneda->moneda->descripcion}} | {{$cuenta->moneda->moneda->simbolo}}</li>
      </ul>
      <div class="text-center">
        @if(count($cuentas) >= 2 && $monedas != 0)
        <a class="btn btn-default" href="/transacciones/{{$cuenta->id}}/traslado"><i class="fa fa-exchange" aria-hidden="true"></i>Traspaso</a>
        @endif
        <a class="btn btn-info" href="{{ route('cuentas.show',$cuenta->id) }}"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
        <a class="btn btn-warning" href="{{ route('cuentas.edit',$cuenta->id) }}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
        {!! Form::open(['method' => 'DELETE','route' => ['cuentas.destroy', $cuenta->id],'style'=>'display:inline']) !!}
        <button type="submit" class="btn btn-danger">
          <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
        </button>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@endforeach

  <div class="text-center">
    <a href="{{ url('cuentas/create') }}" class="btn btn-default"><i class="fa fa-plus-circle fa-3x" aria-hidden="true"></i><h3>Agregar Nueva Cuenta</h3></a>
  </div>
  <div class="text-center">
    {{ $cuentas->links() }}
  </div>

@endsection
