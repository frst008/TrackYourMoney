@extends('layouts.app')
@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Ver Cuenta</div>
				<div class="panel-body">
					@if(is_null($cuenta))
					<h1>Mostrando Categoría</h1>
					<p>La Categoría solicitada no existe</p>
					@else
					<h1>Mostrando: {{ $cuenta->nombre_corto }}</h1>
					<div class="row">
						<div class="col-md-5">
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1">Moneda: </span>
								<input readonly="true" type="text" class="form-control" value="{{ $moneda->descripcion }}">
							</div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-5">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon2">Nombre Corto: </span>
						<input readonly="true" type="text" class="form-control" value="{{ $cuenta->nombre_corto }}">
					</div>
				</div>
			</div>
					<br>
					<div class="row">
						<div class="col-md-5">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon3">Descripción: </span>
						<input readonly="true" type="text" class="form-control" value="{{ $cuenta->descripcion }}">
					</div>
				</div>
			</div>
					<br>
					<div class="row">
						<div class="col-md-5">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon4">Saldo Inicial: </span>
						<input readonly="true" type="text" class="form-control" value="{{ $cuenta->saldo_inicial }}">
					</div>
				</div>
			</div>
					<br>
					<div class="row">
						<div class="col-md-5">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon5">Icono: </span>
						<input readonly="true" type="text" class="form-control" value="{{ $cuenta->icono }}">
					</div>
				</div>
			</div>
					<br>
					@endif
				</div>
			</div>
			<div class="row">
				<div class="col-md-5">
					<a href="{{ url('/cuentas') }}"
					class='btn btn-default btn-sm'>
					Ver todas las cuentas
				</a>
			</div>
		</div>
	</div>
</div>
</div>
</div>

@endsection
