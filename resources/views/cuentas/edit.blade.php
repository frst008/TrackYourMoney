@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">Editar Cuenta</div>

        <div class="panel-body">
          <form form class="form-horizontal" role="form"  action="/cuentas/{{$cuenta->id}}" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="_method" value="PUT">
            {{ csrf_field() }}
            <div class="form-group">
              <div class="col-md-4">
              <select name="moneda_id" class="form-control">
                @foreach($usuario_monedas as $usuario_moneda)
                    <option value="{{$usuario_moneda->id}}"  @if($usuario_moneda->id==$cuenta->moneda_id) selected='selected' @endif >{{$usuario_moneda->moneda->descripcion}}-{{$usuario_moneda->moneda->simbolo}}</option>
                @endforeach
              </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-4">
                <label for="nombre_corto">Nombre Corto</label>
                <input type="text" class="form-control" name="nombre_corto" value="{{$cuenta->nombre_corto}}">
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-4">
                <label for="descripcion">Descripcion</label>
                <input type="text" class="form-control" name="descripcion" value="{{$cuenta->descripcion}}">
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-4">
                <label for="saldo_inicial">Saldo Inicial</label>
                <input type="text" class="form-control" name="saldo_inicial" value="{{$cuenta->saldo_inicial}}">
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-4">
                <label for="icono">Icono</label>
                <input type="file" name="icono" class="form-control" id='image' value="image/{{$cuenta->icono}}">
              </div>
            </div>
            <div class="col-md-6 col-md-offset-4">
              <button type="submit" class="btn btn-primary  pull-right">
                Guardar
              </button>
            </div>
          </form>
          <a href="{{  URL::asset('cuentas') }}"><button class="btn btn-warning pull-right" >Volver</button></a>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
