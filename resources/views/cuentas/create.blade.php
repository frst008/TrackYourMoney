@extends('layouts.app')
@section('content')
<div class="col-lg-10 col-md-offset-1">
  <div class="panel panel-default">
    <div class="panel-heading">Crear Cuenta</div>
    <div class="panel-body">
      <form form class="form-horizontal" role="form" action="/cuentas" method="POST" enctype="multipart/form-data">
      {{ csrf_field() }}
      <div class="form-group">
        <div class="col-md-4">
          <label for="moneda_id">Moneda</label>
          <select name="moneda_id" class="form-control">
            @foreach($monedas as $moneda)
            <option value="{{$moneda->id}}">{{$moneda->moneda->descripcion}}-{{$moneda->moneda->simbolo}}</option>
            @endforeach
          </select>
        </div>
      </div>
      <div class="form-group">
        <div class="col-md-4">
          <label for="nombre_corto">Nombre Corto</label>
          <input type="text" class="form-control" name="nombre_corto">
        </div>
      </div>
      <div class="form-group">
        <div class="col-md-4">
          <label for="descripcion">Descripcion</label>
          <input type="text" class="form-control" name="descripcion">
        </div>
      </div>
      <div class="form-group">
        <div class="col-md-4">
          <label for="saldo_inicial">Saldo Inicial</label>
          <input type="text" class="form-control" name="saldo_inicial">
        </div>
      </div>
      <div class="form-group">
        <div class="col-md-4">
          <label for="icono">Icono</label>
          <input type="file" name="icono" class="form-control" id='image'>
        </div>
      </div>
      <div class="col-md-6 col-md-offset-4">
        <button type="submit" class="btn btn-primary  pull-right">
          Guardar
        </button>
      </div>
    </form>
    <a href="{{ URL::previous() }}"><button class="btn btn-warning pull-left" >Volver</button></a>
  </div>
</div>
</div>
@endsection
