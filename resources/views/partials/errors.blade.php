@if (count($errors) > 0)
<div class="row" id="flash_message">
    <div class="col-md-7 col-md-offset-2 text-center">
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>
</div>
@endif
