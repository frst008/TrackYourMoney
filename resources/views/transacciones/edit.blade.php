@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">Editar Transacción</div>
        <div class="panel-body">
          <form form class="form-horizontal" role="form"  action="/transacciones/{{$transaccion->id}}" method="POST">
            <input type="hidden" name="_method" value="PUT">
            {{ csrf_field() }}
            <div class="form-group">
              <div class="col-md-4">
                <label for="fecha">Fecha</label>
                <input type="date" step="1"  class="form-control" name="fecha" value="{{$transaccion->fecha->toDateString()}}">
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-4">
                <label for="cuenta_id">Cuenta</label>
                <select name="cuenta_id" class="form-control">
                  @foreach($cuentas as $cuenta)
                  <option value="{{$cuenta->id}}" @if($cuenta->id==$transaccion->cuenta_id) selected='selected' @endif >{{$cuenta->nombre_corto}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-4">
                <label for="categoria_id">Categoría</label>
                <select name="categoria_id" class="form-control">
                  @foreach($categorias as $categoria)
                  <option value="{{$categoria->id}}" @if($categoria->id == $transaccion->categoria_id) selected='selected' @endif >{{$categoria->nombre}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-4">
                <label for="detalle">Detalle</label>
                <input type="text" class="form-control" name="detalle" value="{{$transaccion->detalle}}">
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-4">
                <label for="monto">Monto</label>
                <input type="text" class="form-control" name="monto" value="{{$transaccion->monto}}">
              </div>
            </div>
            <div class="col-md-6 col-md-offset-4">
              <button type="submit" class="btn btn-primary  pull-right">
                Guardar
              </button>
            </div>
          </form>
          <a href="{{  URL::asset('transacciones') }}"><button class="btn btn-warning pull-right" >Volver</button></a>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
