@extends('layouts.app')
@section('content')
<div class="col-lg-12">
  <div class="panel panel-default">
    <div class="panel-heading">Transacciones</div>
    <div class="panel-body">
      <table class="table table-hover">
        <tr>
          <td>Tipo</td>
          <td>Fecha</td>
          <td>Cuenta</td>
          <td>Categoría</td>
          <td>Detalle</td>
          <td>Monto</td>
          <td>Acciones</td>
        </tr>
        @foreach($transacciones as $transaccion)
        <tr>
          <td>{{$transaccion->tipo->descripcion}}</td>
          <td>{{$transaccion->fecha->format('d/m/Y')}}</td>
          <td>{{$transaccion->cuenta->nombre_corto}}</td>
          <td>{{$transaccion->categoria->nombre}}</td>
          <td>{{$transaccion->detalle}}</td>
          <td>{{$transaccion->monto}}</td>
          <td>
            <a class="btn btn-info" href="/transacciones/{{$transaccion->id}}"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
            @if($transaccion->traslado != 0)
            <a class="btn btn-primary" href="/transacciones/traslado/{{$transaccion->id}}/edit"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
            @else
            <a class="btn btn-primary" href="/transacciones/{{$transaccion->id}}/edit"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
            @endif
            {!! Form::open(['method' => 'DELETE','route' => ['transacciones.destroy', $transaccion->id],'style'=>'display:inline']) !!}
            <button type="submit" class="btn btn-danger">
                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
            </button>
            {!! Form::close() !!}
          </td>
        </tr>
        @endforeach
      </table>
      <a href="{{ url('transacciones/create') }}"><input type="button" name="name" value="Crear"></input></a>
    </div>
  </div>
</div>
@endsection
