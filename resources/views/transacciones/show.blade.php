@extends('layouts.app')
@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Ver Transacción</div>
				<div class="panel-body">
					@if(is_null($transaccion))
					<h1>Mostrando Categoría</h1>
					<p>La Categoría solicitada no existe</p>
					@else
					<h1>Mostrando: {{ $transaccion->tipo->descripcion }}</h1>
					<div class="row">
						<div class="col-md-5">
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1">Tipo: </span>
								<input readonly="true" type="text" class="form-control" value="{{ $transaccion->tipo->descripcion }}">
							</div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-5">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon2">Fecha: </span>
						<input readonly="true" type="text" class="form-control" value="{{ $transaccion->fecha }}">
					</div>
				</div>
			</div>
					<br>
					<div class="row">
						<div class="col-md-5">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon3">Cuenta: </span>
						<input readonly="true" type="text" class="form-control" value="{{ $transaccion->cuenta->nombre_corto }}">
					</div>
				</div>
			</div>
					<br>
					<div class="row">
						<div class="col-md-5">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon4">Categoría: </span>
						<input readonly="true" type="text" class="form-control" value="{{ $transaccion->categoria->nombre }}">
					</div>
				</div>
			</div>
					<br>
					<div class="row">
						<div class="col-md-5">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon5">Detalle: </span>
						<input readonly="true" type="text" class="form-control" value="{{ $transaccion->detalle }}">
					</div>
				</div>
			</div>
					<br>
					<div class="row">
						<div class="col-md-5">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon5">Monto: </span>
						<input readonly="true" type="text" class="form-control" value="{{ $transaccion->monto }}">
					</div>
				</div>
			</div>
					<br>
					@endif
				</div>
			</div>
			<div class="row">
				<div class="col-md-5">
					<a href="{{ url('/transacciones') }}"
					class='btn btn-default btn-sm'>
					Ver todas las transacciones
				</a>
			</div>
		</div>
	</div>
</div>
</div>
</div>

@endsection
