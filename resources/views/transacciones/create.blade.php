@extends('layouts.app')
@section('content')
<div class="col-lg-10 col-md-offset-1">
  <div class="panel panel-default">
    <div class="panel-heading">Crear Transacción</div>
    <div class="panel-body">
          <form form class="form-horizontal" role="form" action="/transacciones" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
              <div class="col-md-4">
                <label for="fecha">Fecha</label>
                <input type="date" step="1"  class="form-control" name="fecha">
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-4">
              <label for="cuenta_id">Cuenta</label>
              <select name="cuenta_id" class="form-control">
                @foreach($cuentas as $cuenta)
                <option value="{{$cuenta->id}}">{{$cuenta->nombre_corto}}</option>
                @endforeach
              </select>
                </div>
            </div>
            <div class="form-group">
              <div class="col-md-4">
              <label for="categoria_id">Categoría</label>
              <select name="categoria_id" class="form-control">
                @foreach($categorias as $categoria)
                <option value="{{$categoria->id}}">{{$categoria->nombre}}</option>
                @endforeach
              </select>
                </div>
            </div>
            <div class="form-group">
              <div class="col-md-4">
                <label for="detalle">Detalle</label>
                <input type="text" class="form-control" name="detalle">
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-4">
                <label for="monto">Montos</label>
                <input type="text" class="form-control" name="monto">
              </div>
            </div>
            <div class="col-md-6 col-md-offset-4">
              <button type="submit" class="btn btn-primary  pull-right">
                Guardar
              </button>
            </div>
          </form>
          <a href="{{ URL::previous() }}"><button class="btn btn-warning pull-left" >Volver</button></a>
        </div>
      </div>
    </div>
    @endsection
