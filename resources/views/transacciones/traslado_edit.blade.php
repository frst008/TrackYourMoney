@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">Editar transacción traslado </div>
        <div class="panel-body">
          <form class="form-horizontal" role="form" action="/transacciones/traslado/{{$transaccion->id}}/traslado_update" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
              <div class="col-md-4">
                <label for="fecha">Fecha</label>
                <input type="date" step="1"  class="form-control" name="fecha" value="{{$transaccion->fecha->toDateString()}}">
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-4">
                <label for="cuenta_id">Cuenta</label>
                <select name="cuenta_id" class="form-control">
                  @foreach($cuentas as $cuenta)
                  <option value="{{$cuenta->id}}" @if($cuenta->id==$transaccion->cuenta_id) selected='selected' @endif >{{$cuenta->nombre_corto}}</option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-4">
                <label for="detalle">Detalle</label>
                <input type="text" class="form-control" name="detalle" value="{{$transaccion->detalle}}">
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-4">
                <label for="monto">Montos</label>
                <input type="text" class="form-control" name="monto" value="{{$transaccion->monto}}">
              </div>
            </div>
            <div class="col-md-6 col-md-offset-4">
              <button type="submit" class="btn btn-primary  pull-right">
                Guardar
              </button>
            </div>
          </form>
          <a href="{{ URL::previous() }}"><button class="btn btn-warning pull-left" >Volver</button></a>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
