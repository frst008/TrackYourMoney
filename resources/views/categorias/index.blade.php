@extends('layouts.app')
@section('content')
@foreach($categorias as $categoria)
<div class="col-sm-6 col-md-4">
  <div class="thumbnail">
    <img src="image/{{$categoria->icono}}" alt="" />
    <div class="caption">
      <h3 class="text-center">{{$categoria->nombre}}</h3>
      <ul>
        <li>Categoria Padre: {{$categoria->parent['nombre'] ? $categoria->parent['nombre'] : 'N/A' }}</li>
        <li>Tipo: {{$categoria->tipo->descripcion}}</li>
        <li>Presupuesto: {{$categoria->presupuesto}}</li>
      </ul>
      <div class="text-center">
        <a  class="btn btn-info" href="/categorias/{{$categoria->id}}"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
        <a  class="btn btn-warning" href="/categorias/{{$categoria->id}}/edit"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
        {!! Form::open(['method' => 'DELETE','route' => ['categorias.destroy', $categoria->id],'style'=>'display:inline']) !!}
        <button type="submit" class="btn btn-danger">
            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
        </button>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@endforeach

  <div class="text-center">
    <a href="{{ url('categorias/create') }}" class="btn btn-default"><i class="fa fa-plus-circle fa-3x" aria-hidden="true"></i><h3>Agregar Nueva Categoria</h3></a>
  </div>
  <div class="text-center">
    {{ $categorias->links() }}
  </div>

@endsection
