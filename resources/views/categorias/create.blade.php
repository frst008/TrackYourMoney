@extends('layouts.app')
@section('content')
<div class="col-lg-10 col-md-offset-1">
  <div class="panel panel-default">
    <div class="panel-heading">Crear Categorias</div>
    <div class="panel-body">
      <form form class="form-horizontal" role="form" action="/categorias" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group">
          <div class="col-md-4">
            <label for="nombre_corto">Nombre:</label>
            <input type="text" class="form-control" name="nombre">
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-4">
            <label for="categoria_id">Sub-Categoria</label>
            <select name="categoria_id" class="form-control">
              @foreach($categorias as $categoria)
              <option value="{{$categoria->id}}">{{$categoria->nombre}}</option>
              @endforeach
              <option value="">Ninguno</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-4">
            <label for="categoria_id">Tipo</label>
            <select name="tipo_id" class="form-control">
              @foreach($tipos as $tipo)
              <option value="{{$tipo->id}}">{{$tipo->descripcion}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-4">
            <label for="presupuesto">Presupuesto</label>
            <input type="text" class="form-control" name="presupuesto">
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-4">
            <label for="icono">Icono</label>
            <input type="file" name="icono" class="form-control" id='image'>
          </div>
        </div>
        <div class="col-md-6 col-md-offset-4">
          <button type="submit" class="btn btn-primary  pull-right">
            Guardar
          </button>
        </div>
      </form>
      <a href="{{ URL::previous() }}"><button class="btn btn-warning pull-left" >Volver</button></a>
    </div>
  </div>
</div>
@endsection
