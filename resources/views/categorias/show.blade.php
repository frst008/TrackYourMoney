@extends('layouts.app')
@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Ver Categoría</div>
				<div class="panel-body">
					@if(is_null($categoria))
					<h1>Mostrando Categoría</h1>
					<p>La Categoría solicitada no existe</p>
					@else
					<h1>Mostrando: {{ $categoria->nombre }}</h1>
					<div class="row">
						<div class="col-md-5">
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1">Nombre: </span>
								<input readonly="true" type="text" class="form-control" value="{{ $categoria->nombre }}">
							</div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-5">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon2">Sub-Categoria: </span>
						<input readonly="true" type="text" class="form-control" value="{{ $categoria->parent['nombre'] }}">
					</div>
				</div>
			</div>
					<br>
					<div class="row">
						<div class="col-md-5">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon3">Tipo: </span>
						<input readonly="true" type="text" class="form-control" value="{{ $categoria->tipo->descripcion }}">
					</div>
				</div>
			</div>
					<br>
					<div class="row">
						<div class="col-md-5">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon4">Presupuesto: </span>
						<input readonly="true" type="text" class="form-control" value="{{ $categoria->presupuesto }}">
					</div>
				</div>
			</div>
					<br>
					<div class="row">
						<div class="col-md-5">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon5">Icono: </span>
						<input readonly="true" type="text" class="form-control" value="{{ $categoria->icono }}">
					</div>
				</div>
			</div>
					<br>
					@endif
				</div>
			</div>
			<div class="row">
				<div class="col-md-5">
					<a href="{{ url('/categorias') }}"
					class='btn btn-default btn-sm'>
					Ver todas las categorías
				</a>
			</div>
		</div>
	</div>
</div>
</div>
</div>

@endsection
