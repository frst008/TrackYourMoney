<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>TrackYourMoney</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
<link href="/css/styles.css" rel="stylesheet">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" ></script>
<link rel="stylesheet" href="/css/datepicker.css" media="screen" charset="utf-8">
<script src="/js/bootstrap-datepicker.js" charset="utf-8"></script>
@section('header')
  <!-- jQuery & Twitter-Bootstrap
  <script src="{{ asset('js/jquery.min.js') }}"></script>
  <script src="{{ asset('js/bootstrap.min.js') }}"></script>-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
@show
</head>
<body id="app-layout">

  <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{ url('/home') }}"><span>Track</span>YourMoney</a>
				<ul class="user-menu">
        @if (Auth::guest())
            <li><a href="{{ url('/login') }}">Login</a></li>
            <li><a href="{{ url('/register') }}">Register</a></li>
        @else
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user" aria-hidden="true"></i> {{ Auth::user()->name }} <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="{{ url('/logout') }}"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a></li>
						</ul>
					</li>
          @endif
				</ul>
			</div>

		</div><!-- /.container-fluid -->
	</nav>

	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<ul class="nav menu" style="margin-top: 20px;">
			<li><a href="{{ url('/cuentas') }}"><i class="fa fa-credit-card fa-2x" aria-hidden="true"></i> Cuentas</a></li>
			<li><a href="{{ url('/monedas') }}"><i class="fa fa-money fa-2x" aria-hidden="true"></i> Monedas</a></li>
			<li><a href="{{ url('/categorias') }}"><i class="fa fa-briefcase fa-2x" aria-hidden="true"></i> Categorias</a></li>
			<li><a href="{{ url('/transacciones') }}"><i class="fa fa-exchange fa-2x" aria-hidden="true"></i> Transacciones</a></li>
			<li><a href="{{ url('/consultas') }}"><i class="fa fa-newspaper-o fa-2x" aria-hidden="true"></i> Consultas</a></li>
      <li class="parent">
				<a href="#">
					<span data-toggle="collapse" href="#sub-item-1"><i class="fa fa-pie-chart fa-2x" aria-hidden="true"></i><use xlink:href="#stroked-chevron-down"></use></span> Graficos
				</a>
				<ul class="children collapse" id="sub-item-1">
					<li>
						<a class="" href="/categorias/graphic/{{1}}">
							<i class="fa fa-arrow-circle-o-down fa-2x" aria-hidden="true"></i> Ingresos
						</a>
					</li>
					<li>
						<a class="" href="/categorias/graphic/{{2}}">
							<i class="fa fa-arrow-circle-o-up fa-2x" aria-hidden="true"></i> Gastos
						</a>
					</li>
				</ul>
			</li>
    </ul>

	</div><!--/.sidebar-->




  <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main" style="margin-top: 1%;">
    @include('partials.messages')
    @include('partials.errors')
		<div class="row">

        @yield('content')

		</div><!--/.row-->

	</div>	<!--/.main-->




  @section('footer')
    <!-- JavaScripts -->
    <script type="text/javascript">
        $('div.alert').not('.alert-important').delay(3000).fadeOut(300);
    </script>
  @show
</body>
</html>
