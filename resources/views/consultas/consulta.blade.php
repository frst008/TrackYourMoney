@extends('layouts.app')
@section('content')
<div class="text-center">
<h1>{{$detalle}}</h1>
</div>
<br>
<div class="col-lg-12">
  <div class="col-md-6">
    <div class="panel panel-teal">
      <div class="panel-heading dark-overlay">Ingresos:{{$filtro['suma_ingresos']}}</div>
      <div class="panel-body">
        @foreach($filtro['ingresos'] as $ingresos)
        <ul>
          <li>
            Detalle: {{$ingresos->detalle}} |
            Cuenta: {{$ingresos->cuenta->nombre_corto}} |
            Monto: {{$ingresos->monto}}
          </li>
        </ul>
        @endforeach
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel panel-red">
      <div class="panel-heading dark-overlay">Gastos:{{$filtro['suma_gastos']}}</div>
      <div class="panel-body">
        <ul>
          @foreach($filtro['gastos'] as $gastos)
          <li>
            {{$gastos->detalle}}-
            {{$gastos->monto}}-
            {{$gastos->cuenta->nombre_corto}}
          </li>
          @endforeach
        </ul>
      </div>
    </div>
  </div>
</div>
@endsection
