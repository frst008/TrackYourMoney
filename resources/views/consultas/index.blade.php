@extends('layouts.app')
@section('header')
@stop
@section('content')
<div class="col-lg-12">
  <div class="panel panel-default">
    <div class="panel-heading">Transacciones</div>
    <div class="panel-body">
      <form action="/consultas/consulta" method="post">
        {{ csrf_field() }}
        <div class="row">
          <div class="form-group col-md-5">
            <label for="sel1">Seleccione el tipo de consulta:</label>
            <select name="tipo_id" id="tipo_id" class="form-control">
              <option>Tipo de consulta a realizar</option>
              <option value="1">Entre Fechas</option>
              <option value="2">Mes Anterior</option>
              <option value="3">Año Anterior</option>
              <option value="4">Por Año</option>
              <option value="5">Por Mes</option>
            </select>
          </div>
          <div id="entreFechas" style="display:none;" class="form-group col-md-6">
            <label>Fecha inicial:</label><input type="date" name="fecha_1" class="form form-control"/>
            <label>Fecha final:</label><input type="date" name="fecha_2" class="form form-control"/>
            <br>
            <input type="submit" class='btn btn-primary pull-right' value="Buscar">
          </div>
          <div id="submit" style="display:none;" class="form-group col-md-6">
            <br>
            <label>Iniciar busqueda:</label>
            <input type="submit" class='btn btn-primary' value="Buscar">
          </div>
          <div id ="anno" style="display:none;" class="form-group col-md-6">
            <label>Busqueda por año calendario:</label>
            <input type="text" name="anno" id="datepicker"  class="form form-control"/>
            <br>
            <input type="submit" class='btn btn-primary pull-right' value="Buscar">
          </div>
          <div id ="mes" style="display:none;" class="form-group col-md-6">
            <label>Busqueda por mes calendario:</label>
            <input type="text" name ="mes" id="datepicker2" class="form form-control col-md-2"/>
            <br>
            <input type="submit" class='btn btn-primary pull-right' value="Buscar">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">

hola = $("#datepicker").datepicker( {
  format: " yyyy",
  viewMode: "years",
  minViewMode: "years"
});

hola2 = $("#datepicker2").datepicker( {
  format: " mm-yyyy",
  viewMode: "months",
  minViewMode: "months"
});


$('#tipo_id').on('change',function(){
  if($(this).val()==="1"){
    $("#entreFechas").show()
  }
  else{
    $("#entreFechas").hide()
  }

  if ($(this).val()==="2" || $(this).val()==="3") {
    $("#submit").show()
  }
  else{
    $("#submit").hide()
  }

  if ($(this).val()==="4") {
    hola;
    $("#anno").show()
  }
  else{
    $("#anno").hide()
  }

  if ($(this).val()==="5") {
    hola;
    $("#mes").show()
  }
  else{
    $("#mes").hide()
  }
});
</script>
@endsection
