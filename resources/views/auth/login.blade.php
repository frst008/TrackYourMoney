<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>TrackYourMoney</title>

  <!-- CSS -->
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="assets/css/form-elements.css">
  <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
  <!-- Top content -->
  <div class="top-content">
    <div class="inner-bg">
      <div class="container">
        <div class="row">
          <div class="col-sm-8 col-sm-offset-2 text">
            <h1><strong>TrackYourMoney</strong></h1>
            <div class="description">
              <p>
                Página para administar tus cuentas bancarias.
              </p>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-5">
            <div class="form-box">
              <div class="form-top">
                <div class="form-top-left">
                  <h3>Iniciar sesión</h3>
                  <p>Ingresa tu correo electronico y contraseña para iniciar sesión:</p>
                </div>
                <div class="form-top-right">
                  <i class="fa fa-lock"></i>
                </div>
              </div>
              <div class="form-bottom">
                <form role="form" method="POST" action="{{ url('/login') }}" class="login-form">
                  {{ csrf_field() }}
                  <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label class="sr-only" for="email">Correo</label>
                    <input type="email" name="email" placeholder="Correo..." class="form-username form-control" id="email" value="{{ old('email') }}">
                    @if ($errors->has('email'))
                    <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                  </div>
                  <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label class="sr-only" for="password">Contraseña</label>
                    <input type="password" name="password" placeholder="Contraseña..." class="form-password form-control" id="password">
                    @if ($errors->has('password'))
                    <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                  </div>
                  <button type="submit" class="btn">Sign in!</button>
                </form>
              </div>
            </div>

            <div class="social-login">
              <h3>...O inicia sesión desde:</h3>
              <div class="social-login-buttons">
                <a class="btn btn-link-2" href="redirect/facebook">
                  <i class="fa fa-facebook"></i> Facebook
                </a>
                <a class="btn btn-link-2" href="redirect/google">
                  <i class="fa fa-google-plus"></i> Google+
                </a>
              </div>
            </div>

          </div>

          <div class="col-sm-1 middle-border"></div>
          <div class="col-sm-1"></div>

          <div class="col-sm-5">

            <div class="form-box">
              <div class="form-top">
                <div class="form-top-left">
                  <h3>Registrate</h3>
                  <p>Llena la información para trabajar con nosotros:</p>
                </div>
                <div class="form-top-right">
                  <i class="fa fa-pencil"></i>
                </div>
              </div>
              <div class="form-bottom">
                <form class="registration-form" role="form" method="POST" action="{{ url('/register') }}">
                  {{ csrf_field() }}
                  <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="sr-only" for="name">Nombre</label>
                    <input type="text" name="name" placeholder="Nombre..." class="form-first-name form-control" id="name" value="{{ old('name') }}">
                    @if ($errors->has('name'))
                    <span class="help-block">
                      <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                  </div>
                  <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label class="sr-only" for="email">Correo</label>
                    <input type="email" name="email" placeholder="Correo..." class="form-email form-control" id="email" value="{{ old('email') }}">
                    @if ($errors->has('email'))
                    <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                  </div>
                  <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label class="sr-only" for="password">Contraseña</label>
                    <input id="password" placeholder="Contraseña..." type="password" class="form-password form-control" name="password">
                    @if ($errors->has('password'))
                    <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                  </div>

                  <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <label for="password-confirm"  class="sr-only">Confirmar Contraseña</label>
                    <input id="password-confirm" placeholder="Confirmar Contraseña..." type="password" class="form-password form-control" name="password_confirmation">

                    @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                      <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                    @endif

                  </div>
                  <button type="submit" class="btn">Sign me up!</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
