<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cuenta extends Model
{

  protected $table = 'cuentas';
  protected $fillable = [
    'usuario_id' ,
    'moneda_id',
    'nombre_corto',
    'descripcion',
    'saldo_inicial',
    'icono',
  ];

  public function moneda()
  {
    return $this->belongsTo('App\UsuarioMoneda');
  }
}
