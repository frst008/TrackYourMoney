<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Moneda extends Model
{
  protected $table = 'monedas';
  protected $fillable = ['nombre_corto', 'simbolo','descripcion',];

}
