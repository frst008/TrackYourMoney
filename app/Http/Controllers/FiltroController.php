<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Transaccion;
use Carbon\Carbon;

class FiltroController extends Controller
{
  protected $transaccion;
  /**
  * Constructor para manejar las funciones que tiene Transaccion
  */
  public function __construct(Transaccion $transaccion)
  {
    $this->transaccion = $transaccion;
    $this->middleware('auth');
  }
  /**
   * Index de la vista de los filtros
   */
  public function index()
  {
    return view('consultas.index');
  }
  /**
   * Funcion que verifica que tipo de consulta desea
   * realizar el usuario, dependiendo del tipo, va
   * a hacer la consulta al modelo de Transaccion
   * a las funciones mes, anno y filtro.
   * Retorna la vista de "consultas.consulta" con los datos
   * deseados.
   */
  public function consulta(Request $request)
  {
    $actual = Carbon::now();
    $mes    = Carbon::now()->subDays(30);
    $anno   = Carbon::now()->subDays(365);
    $tipo   = $request->input('tipo_id');
    $detalle = '';
    switch ($tipo) {
        case 1:
            $filtro = $this->transaccion->filtro($request->fecha_1,$request->fecha_2);
            $detalle = 'Entre Fechas';
            break;
        case 2:
            $filtro = $this->transaccion->filtro($mes, $actual);
            $detalle = 'Mes actual';
            break;
        case 3:
            $filtro = $this->transaccion->filtro($anno, $actual);
            $detalle = 'Año actual';
            break;
        case 4:
            $filtro = $this->transaccion->anno($request->anno);
            $detalle = 'Año calendario';
            break;
        case 5:
            $mes = $request->mes;
            $fecha = explode(" ", $mes);
            $filtro = $this->transaccion->mes($fecha[2], $fecha[1]);
            $detalle = 'Mes calendario';
            break;
    }
    return view('consultas.consulta', compact('filtro', 'detalle'));
  }
}
