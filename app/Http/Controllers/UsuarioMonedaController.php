<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Message;
use App\Http\Requests;
use App\UsuarioMoneda;
use App\Moneda;
use Auth;
use Validator;

class UsuarioMonedaController extends Controller
{
  protected $usuario_moneda, $moneda;

  public function __construct(UsuarioMoneda $usuario_moneda, Moneda $moneda)
  {
    $this->middleware('auth');
    $this->$usuario_moneda = $usuario_moneda;
    $this->moneda = $moneda;
  }
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $user = Auth::user();
    $usuario_monedas = UsuarioMoneda::where('usuario_id', $user->id)->paginate(6);
    //  $usuario_monedas = UsuarioMoneda::all();//$this->usuario_moneda->where('usuario_id' , $user->id)->get();
    return view('monedas.index', compact('usuario_monedas'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    $principal = UsuarioMoneda::where('moneda_principal', 1)
    ->where('usuario_id', Auth::user()->id)
    ->count();
    $monedas = Moneda::all();


    return view('monedas.create', compact('monedas', 'principal'));
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'tasa' => 'numeric|between:1,9999.99',
    ]);

      if ($validator->fails()) {
        return redirect('monedas/create')->withErrors($validator)->withInput();
      }
    $moneda = new UsuarioMoneda($request->all());
    $moneda->moneda_principal = $request->moneda_principal ? 1 : 0;
    if ($request->moneda_principal) {
      $moneda->tasa = 1;
    }

    $moneda->usuario_id = Auth::user()->id;
    $moneda->save();
    return redirect('monedas');
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    $usuario_moneda  = UsuarioMoneda::find($id);
    return view('monedas.show',compact('usuario_moneda'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    $principal = UsuarioMoneda::where('moneda_principal', 1)
    ->where('usuario_id', Auth::user()->id)
    ->count();
    $principal2 = UsuarioMoneda::where('moneda_principal', 1)
    ->where('id', $id)
    ->where('usuario_id', Auth::user()->id)
    ->count();
    $monedas = $this->moneda->all();
    $usuario_moneda = UsuarioMoneda::find($id);
    return view('monedas.edit',compact('usuario_moneda', 'monedas','principal', 'principal2'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    $validator = Validator::make($request->all(), [
      'tasa' => 'numeric|between:1,9999.99',
    ]);

      if ($validator->fails()) {
        return redirect('monedas/'.$id.'/edit')->withErrors($validator)->withInput();
      }

    $user = Auth::user();
    $usuario_moneda = UsuarioMoneda::find($id);
    $usuario_moneda->usuario_id = $user->id;
    $usuario_moneda->tasa = $request->tasa;
    $usuario_moneda->moneda_id = $request->moneda_id;

    $usuario_moneda->moneda_principal = $request->moneda_principal ? 1 : 0;

    if ($request->moneda_principal) {
      $usuario_moneda->tasa = 1;
    }

    $usuario_moneda->save();
    return redirect('monedas');

  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id, Message $message)
  {
    $usuario_moneda = UsuarioMoneda::find($id);
    try {
      $usuario_moneda->delete();
      $message->pushMessage('Moneda ' . $usuario_moneda->moneda->descripcion. ' eliminada exitosamente', 'danger', false);
    } catch (\Illuminate\Database\QueryException $e) {
      $message->pushMessage('Moneda asociada, no se puede eliminada', 'danger', false);
    }
    return redirect('monedas');
  }
}
