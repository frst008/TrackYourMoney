<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\UsuarioMoneda;
use App\Transaccion;
use App\Categoria;
use App\Cuenta;
use App\Tipo;
use Auth;
use DB;
use Carbon\Carbon;
use DateTime;
use Validator;
use App\Helpers\Message;

class TransaccionController extends Controller
{

  public function __construct()
  {
    $this->middleware('auth');
  }
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Message $message)
  {
    $user          = Auth::user();
    $monedas = UsuarioMoneda::where('usuario_id', Auth::user()->id)->where('moneda_principal', 1)->count();
    $categorias = Categoria::where('usuario_id', Auth::user()->id)->count();
    $transacciones = Transaccion::where('usuario_id', $user->id)->get();
    $t= Transaccion::where('usuario_id', $user->id)->count();

    if ($t > 0) {
      return view('transacciones.index', compact('transacciones'));
    }
    elseif ($monedas == 0) {
        $message->pushMessage('Defina una moneda principal para hacer una transaccion', 'info', false);
        return redirect('monedas');
    }elseif ($categorias == 0) {
      $message->pushMessage('Creé una categoria para realizar una transacción', 'info', false);
      return redirect('categorias');
    }
    return view('transacciones.index', compact('transacciones'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create(Message $message)
  {
    $monedas = UsuarioMoneda::where('usuario_id', Auth::user()->id)->where('moneda_principal', 1)->count();
    $categoria = Categoria::where('usuario_id', Auth::user()->id)->count();
if ($monedas == 0) {
        $message->pushMessage('Defina una moneda principal para hacer una transaccion', 'info', false);
        return redirect('monedas');
    }elseif ($categoria == 0) {
      $message->pushMessage('Creé una categoria para realizar una transacción', 'info', false);
      return redirect('categorias');
    }
    $user       = Auth::user();
    $tipos      = Tipo::all();
    $cuentas    = Cuenta::where('usuario_id', $user->id)->get();
    $categorias = Categoria::where('usuario_id', $user->id)->get();
    return view('transacciones.create', compact('categorias', 'tipos','cuentas'));
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'detalle' => 'required',
      'monto' => 'required',
      'fecha' => 'required'
    ]);

      if ($validator->fails()) {
        return redirect('transacciones/create')->withErrors($validator)->withInput();
      }


    $user = Auth::user();
    $categoria = Categoria::find($request->categoria_id);
    $transaccion = new Transaccion;
    $transaccion->cuenta_id = $request->cuenta_id;
    $transaccion->categoria_id = $request->categoria_id;
    $transaccion->tipo_id = $categoria->tipo_id;
    $transaccion->monto = $request->monto;
    if (($transaccion->tipo_id == 2 && $request->monto > -1) ||($transaccion->tipo_id == 1 && $request->monto < 0)) {
      $transaccion->monto = -1 * $request->monto;
    }
    $transaccion->fecha = $request->fecha;
    if ($request->fecha == "") {
      # code...
      $transaccion->fecha = Carbon::now();
    }

    $transaccion->detalle = $request->detalle;
    $transaccion->usuario_id = $user->id;
    $this->calculo($transaccion->cuenta_id,$request->monto,$transaccion->tipo_id);
    $transaccion->save();
    return redirect('transacciones');
  }

  public function calculo($cuenta_id,$monto,$tipo_id)
  {

    $cuenta = Cuenta::find($cuenta_id);
    if ($tipo_id == 1 && $monto > -1) {
      $cuenta->saldo_inicial += $monto;
    }else if($tipo_id == 1 && $monto < 0) {
      $cuenta->saldo_inicial += ($monto * -1);
    }else if ($tipo_id == 2 && $monto > -1) {
      $cuenta->saldo_inicial -= $monto;
    }else if($tipo_id == 2 && $monto < 0) {
      $cuenta->saldo_inicial -= ($monto * -1);
    }
    $cuenta->save();
  }

  public function traslado($id)
  {
    $user    = Auth::user();
    $cuentas = Cuenta::where('usuario_id', $user->id)->get()->except($id);
    return view('transacciones.traslado',compact('id','cuentas'));
  }

  public function tstore($id,Request $request)
  {
    $validator = Validator::make($request->all(), [
      'detalle' => 'required',
      'monto' => 'required',
      'fecha' => 'required'
    ]);

      if ($validator->fails()) {
        return redirect('transacciones/'.$id.'/traslado')->withErrors($validator)->withInput();
      }
    $monedaLocal = UsuarioMoneda::where('moneda_principal',1)->first();
    $varLocal = (float)$monedaLocal->tasa;
    $cuenta = Cuenta::find($id);
    $moneda1 = UsuarioMoneda::find($cuenta->moneda_id);
    $var1 = (float)$moneda1->tasa;
    $cuenta = Cuenta::find($request->cuenta_id);
    $moneda2 = UsuarioMoneda::find($cuenta->moneda_id);
    $var2 = (float)$moneda2->tasa;
    $user = Auth::user();
    $tran = Transaccion::all();
    $cont = Count($tran);
    if ($cont == 0) {
      $cont=1;
    }
    for ($i=1; $i <= 2; $i++) {
      $transaccion = new Transaccion;
      $transaccion->categoria_id = 1;
      $transaccion->tipo_id = $i;
      $transaccion->detalle = $request->detalle;
      $transaccion->fecha = $request->fecha;
      $transaccion->usuario_id = $user->id;
      $transaccion->traslado = $cont;
        $result = 0;
        if (($transaccion->tipo_id == 2 && (float)$request->monto > 0) || ($transaccion->tipo_id == 1 && (float)$request->monto < 0)) {
          $result = -1 *(float)$request->monto;
        }else {
          $result = (float)$request->monto;
        }
      if ($i == 1) {
        if ($moneda1->id != $moneda2->id) {

          $result2 = $this->conversion($varLocal,$var1,$var2,$result);
          $transaccion->monto = (float)$result2;
        }else {
          $transaccion->monto = (float)$result;
        }
        $transaccion->cuenta_id = $request->cuenta_id;
        $this->calculo($transaccion->cuenta_id,$transaccion->monto,$i);
      }else {
        $transaccion->cuenta_id = $id;
        $transaccion->monto = (float)$result;
        $this->calculo($id,$transaccion->monto,$i);
      }

      $transaccion->save();
    }
    return redirect('transacciones');
  }
  /**
  * var1 va a ser la cuenta a la que se le va a quitar plata
  * var2 va a ser la cuenta a la que se le va a depositar la plata
  */
  public function conversion($varLocal,$var1, $var2,$monto)
  {
    $total = 0;
    $local = $monto/($varLocal/$var1);
    $total = $local/$var2;
    return $total;
  }
  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    $transaccion  = Transaccion::find($id);
    return view('transacciones.show',compact('transaccion'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    $user          = Auth::user();
    $transaccion   = Transaccion::find($id);
    $cuentas = Cuenta::where('usuario_id', $user->id)->get();
    $categorias = Categoria::where('usuario_id', $user->id)->get();
    return view('transacciones.edit',compact('transaccion', 'categorias', 'tipos','cuentas'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    $validator = Validator::make($request->all(), [
      'detalle' => 'required',
      'monto' => 'required',
      'fecha' => 'required'
    ]);

      if ($validator->fails()) {
        return redirect('transacciones/'.$id.'/edit')->withErrors($validator)->withInput();
      }
    $transaccion = Transaccion::find($id);
    $categoria = Categoria::find($transaccion->categoria_id);
    $this->cuenta_edit($transaccion->cuenta_id,$transaccion->monto,true,$categoria->tipo_id);
    $transaccion->categoria_id = $request->categoria_id;
    $categoria = Categoria::find($transaccion->categoria_id);
    $transaccion->tipo_id = $categoria->tipo_id;
    $transaccion->cuenta_id = $request->cuenta_id;
    if (($transaccion->tipo_id == 2 && $request->monto > -1) ||($transaccion->tipo_id == 1 && $request->monto < 0)) {
      $transaccion->monto = -1*$request->monto;
    }else{
      $transaccion->monto = $request->monto;
    }
    $this->cuenta_edit($transaccion->cuenta_id,$transaccion->monto,false,$transaccion->tipo_id);
    $transaccion->detalle = $request->detalle;
    $transaccion->fecha = $request->fecha;
    $transaccion->save();
    return redirect('transacciones');
  }
  public function cuenta_edit($cuenta,$monto,$var,$tipo)
  {
    if ($monto < 0) {
      $monto *= -1;
    }
    $cuenta = Cuenta::find($cuenta);
    if ($var) {
      if ($tipo == 1) {
        $cuenta->saldo_inicial -=  $monto;
      }else {
        $cuenta->saldo_inicial +=  $monto;
      }
    }else {
      if ($tipo == 1) {
        $cuenta->saldo_inicial +=  $monto;
      }else {
        $cuenta->saldo_inicial -=  $monto;
      }
    }
    $cuenta->save();
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function trasladoedit($id)
  {
    $user    = Auth::user();
    $transaccion   = Transaccion::find($id);
    $transaccion2   = Transaccion::where('traslado',$transaccion->traslado)->get()->except($id)->first();

    $cuentas = Cuenta::where('id',$transaccion2->cuenta_id)->get()->except($id);
    $categorias = Categoria::where('usuario_id', $user->id)->get();
    return view('transacciones.traslado_edit',compact('transaccion','transaccion2', 'categorias','cuentas'));
  }


  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function traslado_update(Request $request, $id)
  {
    $tr = Transaccion::find($id);
    $monedaLocal = UsuarioMoneda::where('moneda_principal',1)->first();
    $varLocal = (int)$monedaLocal->tasa;
    $cuenta = Cuenta::find($tr->cuenta_id);
    $moneda1 = UsuarioMoneda::find($cuenta->moneda_id);
    $var1 = (float)$moneda1->tasa;
    $cuenta = Cuenta::find($request->cuenta_id);
    $moneda2 = UsuarioMoneda::find($cuenta->moneda_id);
    $var2 = (float)$moneda2->tasa;
    $t = Transaccion::find($id);
    $all = Transaccion::where('traslado',$t->traslado)->get();
    for ($i=0; $i < Count($all); $i++) {
      if ($all[$i]->tipo_id == $tr->tipo_id) {
        $transaccion = Transaccion::find($id);
      }else{
        $transaccion = Transaccion::find($all[$i]->id);
      }
      $transaccion->categoria_id = 1;
      $transaccion->detalle = $request->detalle;
      $transaccion->fecha = $request->fecha;
      $result = 0;
      if (($transaccion->tipo_id == 2 && $t->tipo_id == 2)||($transaccion->tipo_id == 1 && $t->tipo_id == 1)) {
        $this->cuenta_update($transaccion->cuenta_id,$transaccion->monto,true,$transaccion->tipo_id);
        $this->cuenta_update($transaccion->cuenta_id,$request->monto,false,$transaccion->tipo_id);
        $result = (float)$request->monto;
      }else{
        $this->cuenta_update($transaccion->cuenta_id,$transaccion->monto,false,$transaccion->tipo_id);
        $result = $this->conversion_update($var1,$var2 ,$request->monto);
        $this->cuenta_update($transaccion->cuenta_id,$result,true,$transaccion->tipo_id);
      }
      if (($transaccion->tipo_id == 2 && (float)$request->monto > 0) || ($transaccion->tipo_id == 1 && (float)$request->monto < 0)) {
        $transaccion->monto = -1 *$result;
      }else {
        $transaccion->monto = $result;
      }
      $transaccion->save();
    }
    return redirect('transacciones');
  }

  /**
  * var1 va a ser la cuenta a la que se le va a quitar plata
  * var2 va a ser la cuenta a la que se le va a depositar la plata
  */
  public function conversion_update($var1,$var2,$monto)
  {
    $total = $monto/($var2/$var1);
    return $total;
  }

  public function cuenta_update($cuenta,$monto,$var,$tipo)
  {
    if ($monto < 0) {
      $monto *= -1;
    }
    $cuenta = Cuenta::find($cuenta);
    if ($var) {
      if ($tipo == 1) {
        $cuenta->saldo_inicial +=  $monto;
      }else {
        $cuenta->saldo_inicial +=  $monto;
      }
    }else {
      if ($tipo == 1) {
        $cuenta->saldo_inicial -=  $monto;
      }else {
        $cuenta->saldo_inicial -=  $monto;
      }
    }
    $cuenta->save();
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    $transaccion = Transaccion::find($id);
    $cuenta = Cuenta::find($transaccion->cuenta_id);
    if ($transaccion->traslado == 0) {
      if ($transaccion->tipo_id  == 1) {
        $cuenta->saldo_inicial -= $transaccion->monto;
      }else {
        $cuenta->saldo_inicial += -1*$transaccion->monto;
      }
    }else {
      $transaccion2 = Transaccion::where('traslado', $transaccion->traslado)->get()->except($id)->first();
      $cuenta2 = Cuenta::find($transaccion2->cuenta_id);
      if ($transaccion2->tipo_id  == 1 && $transaccion->tipo_id == 2) {
        $cuenta2->saldo_inicial -= $transaccion2->monto;
        $cuenta->saldo_inicial += -1*$transaccion->monto;
      }else {
        $cuenta2->saldo_inicial += -1*$transaccion2->monto;
        $cuenta->saldo_inicial -= $transaccion->monto;
      }
      $cuenta2->save();
      $transaccion2->delete();
    }
    $transaccion->delete();
    $cuenta->save();
    return redirect('transacciones');
  }
}
