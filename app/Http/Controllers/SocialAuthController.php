<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SocialAccountService;
use Socialite;
use App\UsuarioMoneda;
use Auth;
class SocialAuthController extends Controller
{
  public function redirect($provider)
  {
    return Socialite::driver($provider)->redirect();
  }

  public function callback(SocialAccountService $service, $provider)
  {
    $user = Auth::user();

    try{

      $user = $service->createOrGetUser(Socialite::driver($provider));
      
      auth()->login($user);
      $monedas = UsuarioMoneda::where('usuario_id', $user->id)->count();

      if ($monedas > 0) {
        return redirect()->to('/home');
      }
      return redirect()->to('/monedas/create');
    }catch(\Exception $e){
      return redirect()->to('/');
    }
  }
}
