<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Cuenta;
use App\Moneda;
use App\UsuarioMoneda;
use Auth;
use Image;
use App\Helpers\Message;
use Validator;

class CuentaController extends Controller
{

  public function __construct()
  {
    $this->middleware('auth');
  }
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Message $message)
  {
    $user = Auth::user();
    $principal = UsuarioMoneda::where('usuario_id', $user->id)->count();
    $monedas = UsuarioMoneda::where('moneda_principal', 1)->count();
    if($principal != 0){
      $cuentas = Cuenta::where('usuario_id', $user->id)->paginate(6);
      return view('cuentas.index', compact('cuentas','monedas'));
    }else {
      $message->pushMessage('No existen monedas, favor crear una antes de crear una cuenta', 'danger', false);
      return redirect('monedas/create');
    }
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    $user = Auth::user();
    $monedas = UsuarioMoneda::where('usuario_id', $user->id)->get();
    return view('cuentas.create', compact('monedas'));
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request, Message $message)
  {
    $validator = Validator::make($request->all(), [
      'moneda_id' => 'required',
      'nombre_corto' => 'required',
      'descripcion' => 'required',
      'saldo_inicial' => 'required',
    ]);

      if ($validator->fails()) {
        return redirect('cuentas/create')->withErrors($validator)->withInput();
      }
    $user = Auth::user();
    $cuenta = new Cuenta;
    $cuenta->moneda_id = $request->moneda_id;
    $cuenta->usuario_id = $user->id;
    $cuenta->nombre_corto = $request->nombre_corto;
    $cuenta->descripcion = $request->descripcion;
    if ($request->saldo_inicial < 0) {
      $cuenta->saldo_inicial = -1*$request->saldo_inicial;
    }else {
      $cuenta->saldo_inicial = $request->saldo_inicial;
    }

    $filename = '';

    if($request->hasFile('icono')){
      $icono = $request->file('icono');
      $filename = time() . '.' . $icono->getClientOriginalExtension();
      Image::make($icono)->resize(320, 150)->save( public_path('image/' . $filename ));
    }

    $cuenta->icono = $filename ? $filename : '1.svg';
    $cuenta->save();
    $message->pushMessage('Cuenta guardada correctamente', 'success', false);

    return redirect('cuentas');
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    $cuenta  = Cuenta::find($id);
    $moneda = Moneda::find($cuenta->moneda_id);
    return view('cuentas.show',compact('cuenta','moneda'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    $user = Auth::user();
    $usuario_monedas = UsuarioMoneda::where('usuario_id', $user->id)->get();
    $cuenta = Cuenta::find($id);
    return view('cuentas.edit',compact('cuenta', 'usuario_monedas'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id, Message $message)
  {
    $validator = Validator::make($request->all(), [
      'moneda_id' => 'required',
      'nombre_corto' => 'required',
      'descripcion' => 'required',
      'saldo_inicial' => 'required',
    ]);

      if ($validator->fails()) {
        return redirect('cuentas/create')->withErrors($validator)->withInput();
      }
    $cuenta = Cuenta::find($id);
    $cuenta->moneda_id = $request->moneda_id;
    $cuenta->nombre_corto = $request->nombre_corto;
    $cuenta->descripcion = $request->descripcion;
    $cuenta->saldo_inicial = $request->saldo_inicial;

    $filename = '';

    if($request->hasFile('icono')){
      $icono = $request->file('icono');
      $filename = time() . '.' . $icono->getClientOriginalExtension();
      Image::make($icono)->resize(320, 150)->save( public_path('image/' . $filename ));
    }

    $cuenta->icono = $filename ? $filename : '1.svg';

    $message->pushMessage('Cuenta ' . $cuenta->nombre_corto . ' editada', 'success', false);
    $cuenta->save();
    return redirect('cuentas');
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id, Message $message)
  {
    $cuenta = Cuenta::find($id);
    try {
      $cuenta->delete();
      $message->pushMessage('Cuenta ' .$cuenta->nombre_corto. '  |  ' .$cuenta->descripcion. ' eliminado', 'warning', false);
    } catch (\Illuminate\Database\QueryException $e) {
          $message->pushMessage('Cuenta asociada, no se puede eliminada', 'danger', false);
    }
    return redirect('cuentas');
  }
}
