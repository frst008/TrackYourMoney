<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Categoria;
use App\Tipo;
use Auth;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use Image;
use App\Helpers\Message;
use Validator;
class CategoriaController extends Controller
{


  public function __construct()
  {
    $this->middleware('auth');
  }
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $user       = Auth::user();
    $categorias = Categoria::where('usuario_id', $user->id)->paginate(6);
    return view('categorias.index', compact('categorias'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    $user       = Auth::user();
    $tipos      = Tipo::all();
    $categorias = Categoria::where('usuario_id', $user->id)->get();
    return view('categorias.create', compact('categorias', 'tipos'));
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request, Message $message)
  {
    $validator = Validator::make($request->all(), [
      'nombre' => 'required',
      'tipo_id' => 'required',

      'presupuesto' => 'numeric|between:1,99999999999.99',
    ]);

      if ($validator->fails()) {
        return redirect('categorias/create')->withErrors($validator)->withInput();
      }
    $user = Auth::user();
    $categoria = new Categoria;
    $categoria->nombre = $request->nombre;
    $categoria->categoria_id = empty($request->categoria_id) ? null : $request->categoria_id;
    $categoria->tipo_id = $request->tipo_id;


    $filename = '';

    if($request->hasFile('icono')){
      $icono = $request->file('icono');
      $filename = time() . '.' . $icono->getClientOriginalExtension();
      Image::make($icono)->resize(320, 150)->save( public_path('image/' . $filename ));
    }

    $categoria->icono = $filename ? $filename : '1.svg';

    $categoria->presupuesto = $request->presupuesto;
    $categoria->usuario_id = $user->id;
    $categoria->save();
    $message->pushMessage('Categoria guardada correctamente', 'success', false);
    return redirect('categorias');
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    $user       = Auth::user();
    $categoria  = Categoria::find($id);
    return view('categorias.show',compact('categoria'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    $user       = Auth::user();
    $categoria  = Categoria::find($id);
    $categorias = Categoria::where('usuario_id', $user->id)->get()->except($id);
    $tipos      = Tipo::all();
    return view('categorias.edit',compact('categoria', 'categorias', 'tipos'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id, Message $message)
  {
    $validator = Validator::make($request->all(), [
      'nombre' => 'required',
      'tipo_id' => 'required',
      'presupuesto' => 'numeric|between:1,99999999999.99',
    ]);

      if ($validator->fails()) {
        return redirect('categorias/'.$id.'/edit')->withErrors($validator)->withInput();
      }
    $categoria = Categoria::find($id);
    $categoria->nombre = $request->nombre;
    $categoria->categoria_id = empty($request->categoria_id) ? null : $request->categoria_id;
    $categoria->tipo_id = $request->tipo_id;

    $filename = '';

    if($request->hasFile('icono')){
      $icono = $request->file('icono');
      $filename = time() . '.' . $icono->getClientOriginalExtension();
      Image::make($icono)->resize(320, 150)->save( public_path('image/' . $filename ));
    }

    $categoria->icono = $filename ? $filename : '1.svg';


    $categoria->presupuesto = $request->presupuesto;

    $categoria->save();
    $message->pushMessage('Categoria ' . $categoria->nombre . ' editada', 'success', false);
    return redirect('categorias');
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id, Message $message)
  {
    $categoria = Categoria::find($id);
      try {
        if ($id != 1) {
          $categoria->delete();
        }
        $message->pushMessage('Categoria ' . $categoria->nombre. ' eliminada', 'danger', false);
      } catch (\Illuminate\Database\QueryException $e) {
        $message->pushMessage('Categoria asociada, no se puede eliminada', 'danger', false);
      }


    return redirect('categorias');
  }

  /**
  * Recuperar los datos para el gráfico
  * @return Array
  */
  protected function getGraphicData($tipo = 1,$cat = 0)
  {
    $user    = Auth::user();
    // "Raw Queries"

    if ($cat == 0) {
      $category = ' is null ';
    }else {
      $category = ' = '.$cat;
    }
    $categorias =
    DB::select(

    DB::raw("select cat.id as cat_id, cat.nombre as categoria_nombre,floor((sum(tr.monto/(1/um.tasa))*100)/t.total) percentage
            from transacciones tr
            join categorias cat on cat.id = tr.categoria_id
        	  join cuentas c on c.id = tr.cuenta_id
            join usuario_monedas um on  um.id = c.moneda_id
            join (select sum(monto) as total from transacciones) t
            where tr.tipo_id =".$tipo." and cat.categoria_id ".$category." and cat.usuario_id =". $user->id ."
            group by tr.categoria_id "));

    foreach ($categorias as $categoria) {
      if ($categoria->percentage < 0) {
        $categoria->percentage *= -1;
      }
    }

    return $categorias;
  }

  /**
  * Retorna JSON con los datos de gráfico
  * @return JSON;
  */
  public function graphicData($tipo = 1,$cat = 0)
  {
    return Response::json($this->getGraphicData($tipo,$cat));
  }

  /**
  * Muestra la vista con el gráfico
  */
  public function graphic($tipo = 1)
  {
    if ($tipo == 1) {
      $title = 'Ingresos';
    }else {
      $title = 'Gastos';
    }
    $type = $tipo;
    $cat = 0;
    $data['graphicData'] = json_encode($this->getGraphicData($type,$cat));
    $data['graphicTitle'] = json_encode(['title'=> $title.' de Categorias']);
    return view('categorias.graphic', $data, compact('title','type'));
  }


}
