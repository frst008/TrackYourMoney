<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Route::auth();

Route::get('/home', 'HomeController@index');
Route::get('/categorias/graphic-data/{tipo}/{cat}', 'CategoriaController@graphicData');
Route::get('/categorias/graphic/{tipo}', 'CategoriaController@graphic');
Route::resource('/monedas','UsuarioMonedaController');
Route::resource('/cuentas','CuentaController');
Route::resource('/categorias','CategoriaController');
Route::resource('/transacciones','TransaccionController');
Route::get('/transacciones/{id}/traslado', 'TransaccionController@traslado');
Route::post('/transacciones/{id}/tstore', 'TransaccionController@tstore');
Route::get('/consultas', 'FiltroController@index');
Route::post('/consultas/consulta', 'FiltroController@consulta');
Route::get('/transacciones/traslado/{id}/edit', 'TransaccionController@trasladoedit');
Route::post('/transacciones/traslado/{id}/traslado_update', 'TransaccionController@traslado_update');






Route::get('/redirect/{provider}', 'SocialAuthController@redirect');
Route::get('/callback/{provider}', 'SocialAuthController@callback');
