<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{

  protected $table = 'categorias';
  protected $fillable = [
    'nombre', 'categoria_id','tipo_id','icono','presupuesto', 'usuario_id',
  ];

  public function parent()
  {
    return $this->belongsTo('App\Categoria','categoria_id');
  }

  public function tipo()
  {
    return $this->belongsTo('App\Tipo','tipo_id');
  }

}
