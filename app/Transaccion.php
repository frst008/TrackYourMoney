<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DateTime;
use Auth;

class Transaccion extends Model
{
  protected $table = 'transacciones';
  protected $dates = ['fecha'];
  protected $fillable = ['id','usuario_id' ,'tipo_id', 'detalle','monto','cuenta_id','categoria_id','fecha'];

  public function tipo()
  {
    return $this->belongsTo('App\Tipo');
  }
  public function cuenta()
  {
    return $this->belongsTo('App\Cuenta');
  }
  public function categoria()
  {
    return $this->belongsTo('App\Categoria');
  }

  public function sumIngresos($ingresos)
  {
    $total = 0;
    for ($i=0; $i < Count($ingresos); $i++) {
      $cuenta = Cuenta::find($ingresos[$i]->cuenta_id);
      $moneda = UsuarioMoneda::find($cuenta->moneda_id);
      $total += $ingresos[$i]->monto/(1/$moneda->tasa);
    }
    return  $total;
  }

  public function sumGastos($gastos)
  {
    $total = 0;
    for ($i=0; $i < Count($gastos); $i++) {
      $cuenta = Cuenta::find($gastos[$i]->cuenta_id);
      $moneda = UsuarioMoneda::find($cuenta->moneda_id);
      $total += (-1*$gastos[$i]->monto)/(1/$moneda->tasa);
    }
    return  $total;
  }

  /**
  * Funciones para hacer los filtros
  */
  public function filtro($var1, $var2)
  {
$user    = Auth::user();
    $ingresos = Transaccion::whereBetween('fecha', array(
      new DateTime($var1),
      new DateTime($var2)))
      ->where('tipo_id', 1)->where('usuario_id', $user->id)
      ->get();

      $suma_ingresos = $this->sumIngresos($ingresos);


      $gastos   = Transaccion::whereBetween('fecha', array(
        new DateTime($var1),
        new DateTime($var2)))
        ->where('tipo_id', 2)->where('usuario_id', $user->id)
        ->get();

        $suma_gastos   = $this->sumGastos($gastos);

        return [
          'ingresos' => $ingresos,
          'gastos'   => $gastos,
          'suma_ingresos' => $suma_ingresos,
          'suma_gastos' => $suma_gastos
        ];
      }

      public function anno($year)
      {
        $user    = Auth::user();
        $ingresos = Transaccion::whereYear('fecha', '=', $year)
        ->where('tipo_id', 1)->where('usuario_id', $user->id)
        ->get();

        $gastos   = Transaccion::whereYear('fecha', '=', $year)
        ->where('tipo_id', 2)->where('usuario_id', $user->id)
        ->get();

        $suma_ingresos = $this->sumIngresos($ingresos);

        $suma_gastos   = $this->sumGastos($gastos);
        return [
          'ingresos' => $ingresos,
          'gastos'   => $gastos,
          'suma_ingresos' => $suma_ingresos,
          'suma_gastos' => $suma_gastos
        ];
      }

      public function mes($year, $month)
      {
        $user    = Auth::user();
        $ingresos = Transaccion::whereYear('fecha', '=', $year)
        ->whereMonth('fecha', '=', $month)
        ->where('tipo_id', 1)->where('usuario_id', $user->id)
        ->get();
        $gastos   = Transaccion::whereYear('fecha', '=', $year)
        ->whereMonth('fecha', '=', $month)
        ->where('tipo_id', 2)->where('usuario_id', $user->id)
        ->get();

        $suma_ingresos = $this->sumIngresos($ingresos);
        $suma_gastos   = $this->sumGastos($gastos);

        return [
          'ingresos' => $ingresos,
          'gastos'   => $gastos,
          'suma_ingresos' => $suma_ingresos,
          'suma_gastos' => $suma_gastos
        ];
      }
    }
