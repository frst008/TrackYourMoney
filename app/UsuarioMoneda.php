<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsuarioMoneda extends Model
{

  protected $table = 'usuario_monedas';
  protected $fillable = ['usuario_id' ,'moneda_id', 'tasa','moneda_principal'];

  public function moneda()
  {
    return $this->belongsTo('App\Moneda');
  }

}
